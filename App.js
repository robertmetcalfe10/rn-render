import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Button} from 'react-native-elements';


export default class App extends Component {

  state = {
      open: false
  }

  openImage = () => {
      console.log(this.state.open);
      if (this.state.open === false) {
        this.setState({open: true}, function () {
            console.log(this.state.open);
        });
    } else {
        this.setState({open: false}, function () {
            console.log(this.state.open);
        });
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <Button
            title="Image"
            text="Image"
            onPress={this.openImage}
            buttonStyle={{margin: 50}}/>
          {
              this.state.open &&
            <Image
                style={{width: 91, height: 132}}
                source={require('./ucdLogo.jpg')}
            />
          }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
